import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  state: {
    isLandscape: true,
    canvasItems: [],
    items: [
      { name: 'Shape Arrow', isShape: true },
      { name: 'Shape Bi-Directional', isShape: true },
      // { name: 'Shape Circle', isShape: true },
      // { name: 'Shape Diamond', isShape: true },
      // { name: 'Shape Hexagon', isShape: true },
      // { name: 'Shape Right Angle Triangle', isShape: true },
      // { name: 'Shape Round Edge', isShape: true },
      // { name: 'Shape Speech Bubble 1', isShape: true },
      // { name: 'Shape Speech Bubble 2', isShape: true },
      // { name: 'Shape Square', isShape: true },
      // { name: 'Shape Star', isShape: true },
      // { name: 'Shape Triangle', isShape: true },
      { name: 'Corn' },
      { name: 'Cow' },
      { name: 'Damaged Habitat' },
      { name: 'Factory' },
      { name: 'Farm' },
      { name: 'Farmer' },
      { name: 'Fish' },
      { name: 'Food Distribution' },
      { name: 'Food Donation' },
      { name: 'Food Storage' },
      { name: 'Globe' },
      { name: 'Population' },
      { name: 'Processor' },
      { name: 'Rice' },
      { name: 'Seeds' },
      { name: 'Tractor' },
      { name: 'Warehouse' },
      { name: 'WFP' },
      { name: 'Wheat' }
    ],
    draggableItem: {},
    selectedItem: {},
    showProperty: false
  },
  mutations: {
    isLandscape (state, status) {
      state.isLandscape = status
    },
    setDraggableItem (state, item) {
      state.draggableItem = item
    },
    setShowProperty (state, item) {
      state.showProperty = true
      state.selectedItem = item
    },
    setHideProperty (state) {
      state.showProperty = false
      state.selectedItem = {}
    },
    appendCloneToCanvasItem (state, item) {
      state.canvasItems.push(item)
    },
    setSelectedItem (state, item) {
      state.selectedItem = item
    },
    deleteItem (state, item) {
      const index = state.canvasItems.indexOf(item)

      state.canvasItems.splice(index, 1)
    },
    modifyItem (state, item) {
      switch (item.type) {
        case 'rotate':
          state.selectedItem.rotation = item.rotation
          break
        case 'scale':
          state.selectedItem.scale = item.scale
          break
      }
    },
    setZIndex (state, index) {
      state.selectedItem.zIndex += index
    },
    changeColor (state, color) {
      state.selectedItem.color = color
    },
    clearCanvas (state) {
      state.canvasItems = []
      state.selectedItem = {}
      state.showProperty = false
    }
  },
  getters: {
    showClearButton (state) {
      return state.canvasItems.length > 0
    }
  }
})
