import Vue from 'vue'
import App from './App'
import store from './store'
import router from './router'
import fontawesome from '@fortawesome/fontawesome'
import solid from '@fortawesome/fontawesome-free-solid'
import regular from '@fortawesome/fontawesome-free-regular'
import smoothscroll from 'smoothscroll-polyfill'

fontawesome.library.add(solid)
fontawesome.library.add(regular)

smoothscroll.polyfill()

Vue.config.productionTip = false

router.beforeEach((to, from, next) => {
  document.title = `${to.name} | Food Security`
  next()
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  template: '<App/>',
  components: { App }
})
