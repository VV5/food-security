import Vue from 'vue'
import Router from 'vue-router'
import Copyright from '@/components/Copyright'
import Credits from '@/components/Credits'
import Introduction from '@/components/Introduction'
import MainContent from '@/components/MainContent'
import Dashboard from '@/components/Dashboard'

Vue.use(Router)

export default new Router({
  base: window.location.pathname,
  routes: [
    {
      path: '/',
      name: 'Copyright',
      component: Copyright
    },
    {
      path: '/credits',
      name: 'Credits',
      component: Credits
    },
    {
      path: '/introduction',
      name: 'Introduction',
      component: Introduction
    },
    {
      path: '/content',
      name: 'Content',
      component: MainContent
    },
    {
      path: '/dashboard',
      name: 'Dashboard',
      component: Dashboard
    }
  ]
})
