# Changelog

## v3.3.0
- Removed sidebar and hamburger navigation button
- Added function to lock the screen to prevent scrolling until the last section has been viewed

## v3.2.0
- Restyled dashboard and many other components
- Fixed reloading of page when page orientation change
- Added function to send object forward or backward
- Optimised webpack configuration for production

## v3.1.1
- Fixed background images path not correctly linked

## v3.1.0
- Cleaned up Vuex Store for items and objects
- Fixed optimisation by replacing Vuex actions for mutations. Hence, all unnecessary `async/await` is removed during this process.
- Removed e2e runner test
- Fixed text background colour changing instead of the text itself

## v3.0.0
- Fixed some minor bugs and overall improvements
- Fixed shapes having colour overlay
- Fixed an error with text not being added to canvas when "Add text" button is clicked

## v2.0.0
- Upgraded to Webpack 4

## v1.1.3
- Added a func to deselect items when not clicking on it
- Updated styling for Contents

## v1.1.2
- Removed scroll snapping as it is not working together with `window.scrollTo()`
- Moved position text box in Panel Discussion
- Fixed layout of instruction to rotate device on mobile screen sizes
- Added colour selection template for basic shapes and text
- Fixed an issue where the font family of the buttons do not match the font family of the body text

## v1.1.1
- Increased font size by slight
- Added a sliding staircase effect to the questions page
- Changed up "Next" button animation and size
- Added scroll snapping for each section in Main Content

## v1.1.0
- Major refactor of the interactive part

## v1.0.0
- Added a function to delete items from the canvas
- Added a function to generate assets from name property in the data set
- Added a function to clear all items on the canvas
- Fixed document title
- Fixed items not being selected upon adding them into the canvas
- Fixed issue with items not smooth dragging properly
