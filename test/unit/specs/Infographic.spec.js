import { shallowMount, createLocalVue } from '@vue/test-utils'
import Router from 'vue-router'
import Infographic from '@/components/Infographic'

const localVue = createLocalVue()

describe('Infographic', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(Infographic)
  })

  test('', () => {
    localVue.use(Router)

    const router = new Router()

    wrapper = shallowMount(Infographic, {router, localVue})

    wrapper.find('button').trigger('click')

    expect(wrapper.vm.$route.path).toBe('/dashboard')
  })
})
