import { createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import store from '@/store'

const localVue = createLocalVue()
localVue.use(Vuex)

describe('store', () => {
  test('should be portrait mode if isLandscape is false', () => {
    expect(store.state.isLandscape).toBe(true)

    store.commit('isLandscape', false)

    expect(store.state.isLandscape).toBe(false)
  })

  test('should hide property and remove item', () => {
    const item = { name: 'Foo' }

    store.commit('setSelectedItem', item)
    store.commit('setShowProperty')

    expect(store.state.selectedItem).not.toEqual({})
    expect(store.state.showProperty).toBe(true)

    store.commit('setHideProperty')

    expect(store.state.showProperty).toBe(false)
    expect(store.state.selectedItem).toEqual({})
  })

  test('should append items to canvas correctly', () => {
    expect(store.state.canvasItems.length).toBe(0)

    const item = {
      name: 'Foo'
    }

    store.commit('appendCloneToCanvasItem', item)

    expect(store.state.canvasItems.length).toBeGreaterThan(0)
    expect(store.state.canvasItems).toContain(item)
  })

  test('should set the rotation of the item', () => {
    const item = {
      type: 'rotate',
      rotation: 90
    }

    store.commit('modifyItem', item)

    expect(store.state.selectedItem.rotation).toBe(item.rotation)
  })

  test('should set the scale of the item', () => {
    const item = {
      type: 'scale',
      scale: 3.0
    }

    store.commit('modifyItem', item)

    expect(store.state.selectedItem.scale).toBe(item.scale)
  })

  test('should change colour for shapes accordingly', () => {
    const color = {
      hex: '000000',
      name: 'black'
    }

    store.commit('changeColor', color)

    expect(store.state.selectedItem.color).toBe(color)
  })

  test('should set selected item to an empty object and hide property', () => {
    const item = { name: 'Foo' }

    store.commit('setSelectedItem', item)
    store.commit('setShowProperty')

    expect(store.state.selectedItem).not.toEqual({})
    expect(store.state.showProperty).toBe(true)

    store.commit('clearCanvas')

    expect(store.state.selectedItem).toEqual({})
    expect(store.state.showProperty).toBe(false)
  })
})
