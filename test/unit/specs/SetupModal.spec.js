import { shallowMount } from '@vue/test-utils'
import SetupModal from '@/components/SetupModal'

describe('SetupModal', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(SetupModal, {
      propsData: {
        isActive: true
      }
    })
  })

  test('modal should close when button is clicked', () => {
    wrapper.find('button.delete').trigger('click')

    expect(wrapper.emitted('closeSetupModal')).toBeTruthy()
  })
})
