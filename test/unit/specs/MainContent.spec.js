import { shallowMount } from '@vue/test-utils'
import MainContent from '@/components/MainContent'

describe('MainContent', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(MainContent, {
      attachToDocument: true,
      data () {
        return {
          isActive: false,
          selectedVideo: null
        }
      }
    })
  })

  test('should close the modal when ESC button is pressed', () => {
    wrapper.setData({ isActive: true })

    wrapper.trigger('keyup', { key: 'Escape' })

    expect(wrapper.vm.isActive).toBe(false)
  })

  test('should not close the modal when buttons other than ESC are pressed', () => {
    wrapper.setData({ isActive: true })

    wrapper.trigger('keyup')

    expect(wrapper.vm.isActive).toBe(true)
  })
})
