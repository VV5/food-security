import { shallowMount } from '@vue/test-utils'
import Preparation from '@/components/Preparation'

describe('Preparation', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(Preparation)
  })

  test('defaults modal as closed', () => {
    expect(wrapper.vm.isActive).toBe(false)
  })

  test('modal should open when button is clicked', () => {
    wrapper.find('button').trigger('click')

    expect(wrapper.vm.isActive).toBe(true)
  })
})
