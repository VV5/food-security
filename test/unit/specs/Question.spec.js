import { shallowMount } from '@vue/test-utils'
import Question from '@/components/Question'

describe('Question', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(Question, {
      attachToDocument: true
    })
  })

  test('should have a set of questions', () => {
    expect(wrapper.vm.questions.length).toBeGreaterThan(0)
  })
})
