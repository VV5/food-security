import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import App from '@/App'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)

describe('App', () => {
  let wrapper
  let store, state, mutations
  let router

  beforeEach(() => {
    state = {
      isLandscape: false,
      version: null
    }

    mutations = {
      isLandscape: jest.fn(),
      setVersion: jest.fn()
    }

    store = new Vuex.Store({
      state, mutations
    })

    router = new VueRouter()

    wrapper = shallowMount(App, {
      attachToDocument: true,
      store,
      router,
      localVue
    })
  })

  test('defaults isLandscape to true', () => {
    window.innerWidth = 1080
    window.innerHeight = 1920

    wrapper.trigger('resize')

    expect(mutations.isLandscape).toHaveBeenCalled()
  })
})
