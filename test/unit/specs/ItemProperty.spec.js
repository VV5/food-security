import { shallowMount, createLocalVue } from '@vue/test-utils'
import sinon from 'sinon'
import Vuex from 'vuex'
import ItemProperty from '@/components/ItemProperty'

const localVue = createLocalVue()
localVue.use(Vuex)

describe('ItemProperty', () => {
  let wrapper, store, state, mutations, methods

  beforeEach(() => {
    state = {}

    mutations = {
      modifyItem: jest.fn(),
      removeSelectedItem: jest.fn()
    }

    store = new Vuex.Store({
      state, mutations
    })

    methods = {
      transformItem: sinon.stub()
    }

    wrapper = shallowMount(ItemProperty, {
      propsData: {
        selectedItem: {
          name: 'Item 1',
          rotation: 0,
          scale: 1.0
        },
        showProperty: true
      },
      store,
      localVue,
      methods
    })
  })

  test('defaults rotation and scale to 0 and 1.0 respectively', () => {
    expect(wrapper.vm.selectedItem.rotation).toBe(0)
    expect(wrapper.vm.selectedItem.scale).toBe(1.0)
  })

  test('rotate item should behave correctly', () => {
    const rotate = wrapper.find('#rotate')
    rotate.element.value = 180
    rotate.trigger('change')

    expect(mutations.modifyItem).toHaveBeenCalled()
    expect(methods.transformItem.called).toBe(true)
  })

  test('scale item should behave correctly', () => {
    const scale = wrapper.find('#scale')
    scale.element.value = 2.0
    scale.trigger('change')

    expect(mutations.modifyItem).toHaveBeenCalled()
    expect(methods.transformItem.called).toBe(true)
  })
})
