import { shallowMount, createLocalVue } from '@vue/test-utils'
import VueRouter from 'vue-router'
import Introduction from '@/components/Introduction'

const localVue = createLocalVue()
localVue.use(VueRouter)

describe('Introduction', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(Introduction)
  })

  test('should navigate to the correct path when button is clicked', () => {
    const router = new VueRouter()

    wrapper = shallowMount(Introduction, {
      router, localVue
    })

    wrapper.find('button').trigger('click')

    expect(wrapper.vm.$route.path).toBe('/content')
  })
})
